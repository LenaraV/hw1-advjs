class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  get userName() {
    return this.name;
  }
  set userName(newName) {
    this.name = newName;
  }
  get userAge() {
    return this.age;
  }
  set userAge(newAge) {
    return this.age;
  }
  get userSalary() {
    return this.salary;
  }

}
class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;

  }
  get lang() {
    return this._lang;
  }
  set lang(newLang) {
    this._langs = newLang;
  }
  get newSalary() {
    return `${this.salary} * 3`;
  }
}
console.log(Employee);
const Programmer1 = new Programmer('Lenara', 35, 10000, 'Ukranian');
const Programmer2 = new Programmer('Anna', 30, 11000, 'Engish');
console.log(Programmer1);
console.log(Programmer2);

